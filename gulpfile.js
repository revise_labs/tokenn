'use strict';

var gulp = require("gulp");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var merge = require("merge-stream");
var exec = require("child_process").exec;

gulp.task("build:facebook", function() {
    var fbAuth = gulp.src('src/facebook-auth.js').pipe(uglify());

    var fbSDK = gulp.src('sdks/facebook.js');

    merge(fbSDK, fbAuth)
        .pipe(concat('facebook-auth.js'))
        .pipe(gulp.dest('build'));

});

gulp.task("build:google", function() {
    var gAuth = gulp.src('src/google-auth.js').pipe(uglify());

    var gSDK = gulp.src('sdks/google.js');

    merge(gSDK, gAuth)
        .pipe(concat('google-auth.js'))
        .pipe(gulp.dest('build'));
});

gulp.task("build", ['build:facebook', 'build:google'], function() {
    return gulp.src(['lib/promise-js.js', 'build/facebook-auth.js', 'build/google-auth.js'])
        .pipe(concat("tokenn.js"))
        .pipe(gulp.dest('build'))
});

gulp.task("default", function() {
    console.log("Compiling all TypeScript files.");
    exec("tsc", function(err) {
        if(err) console.log(err);
        console.log("Done! Use 'npm run ts' to watch *.ts files for compilation.")
    });

});
