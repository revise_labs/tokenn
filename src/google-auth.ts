declare var Promise: any;
declare var gapi: any;

interface GoogleAuthResponse {
    getAuthResponse: any
}

class GoogleAuth {
    //See https://github.com/google/google-api-javascript-client/blob/master/samples/authSample.html for reference

    private _scope: string;
    private auth2: any;

    constructor(private _clientId: string, permissions: string[] = ['profile', 'email']) {
        this._scope = permissions.join(' ');
        console.log(this._clientId);
        gapi.load('auth2', () => {
            gapi.auth2.init({
                client_id: this._clientId,
                scope: this._scope
            }).then(() => {
                this.auth2 = gapi.auth2.getAuthInstance();
                console.log("GAPI Initialized.");
            });
        });
    }

    login() {
        if(this.auth2) {
            return new Promise((resolve) => {
                this.auth2.signIn().then((response: GoogleAuthResponse) => {
                    var authResponse = response.getAuthResponse();
                    resolve({accessToken: authResponse.access_token, idToken: authResponse.id_token});
                });
            });
        } else {
            console.error("Google Auth API not initialized yet.");
        }

    }

}