/**
 * Created by ksheppard on 06/10/2016.
 */

declare var FB: any;
declare var Promise: any;

class FacebookAuth {

    constructor(private _appId: string,
                private _version: string = 'v2.5'
    ) {
        FB.init({
            appId: this._appId,
            version: this._version
        });
    }

    login(permissions: string[] = ['public_profile', 'email']) {
        let scope = permissions.join(',');
        return new Promise((resolve, reject) => {
            FB.login((res) => {
                if(res.status == "not_authorized") reject(res);
                else {
                    resolve({accessToken: res.authResponse.accessToken, userId: res.authResponse.userID});

                }
            }, {scope: scope});
        });

    }


}
