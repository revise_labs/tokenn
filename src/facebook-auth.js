/**
 * Created by ksheppard on 06/10/2016.
 */
var FacebookAuth = (function () {
    function FacebookAuth(_appId, _version) {
        if (_version === void 0) { _version = 'v2.5'; }
        this._appId = _appId;
        this._version = _version;
        FB.init({
            appId: this._appId,
            version: this._version
        });
    }
    FacebookAuth.prototype.login = function (permissions) {
        if (permissions === void 0) { permissions = ['public_profile', 'email']; }
        var scope = permissions.join(',');
        return new Promise(function (resolve, reject) {
            FB.login(function (res) {
                if (res.status == "not_authorized")
                    reject(res);
                else {
                    resolve({ accessToken: res.authResponse.accessToken, userId: res.authResponse.userID });
                }
            }, { scope: scope });
        });
    };
    return FacebookAuth;
})();
